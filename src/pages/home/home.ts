import { Component } from '@angular/core';
import { NavController , ModalController } from 'ionic-angular';
import { ContactPage } from '../contact/contact';
import { LoginPage } from '../loginPage/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public modalCtrl: ModalController) {

  }

  goContact() {
  	let modalCreate = this.modalCtrl.create(ContactPage);
  	modalCreate.present();
  }

  toDoApp() {
    let loginPage = this.modalCtrl.create(LoginPage);
    loginPage.present();
  }
}
