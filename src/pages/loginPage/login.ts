import { Component } from '@angular/core';

@Component({
  templateUrl: 'login.html'
})

export class LoginPage {
  constructor(){}

  items = [];
  value:any;

  addItems() {
    if(this.value !== "" && this.value !== undefined) {
      this.items.push(this.value);
      this.value = "";
    }
  }

  closeToDo(index) {
    this.items.splice(index, 1);
  }
}
