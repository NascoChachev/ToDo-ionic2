import { Component } from '@angular/core';
import { NavController , NavParams , ModalController} from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage {

  constructor(private navCtrl: NavController , public navParams : NavParams , public modalCtrl: ModalController) {}

  ionViewDidLoad() {
 	  console.log(this.navParams.get('thing1'));
  }

  goToHomePage() {
    let modalPage = this.modalCtrl.create(HomePage);
    modalPage.present();
  }
}
